package util;

public class Constants {
	 public static final String lampada = "LAMP";
	 public static final String tv = "TV";
	 public static final String arcondicionado = "AIRCONDITIONER";
	 public static final String radio = "RADIO";
	 public static final String cortina = "CURTAIN";
	 public static final String statusLampada = "LAMPstatus";
	 public static final String statusTv = "TVstatus";
	 public static final String statusArcondicionado = "AIRCONDITIONERstatus";
	 public static final String statusRadio = "RADIOstatus";
	 public static final String statusCortina = "CURTAINstatus";
	 public static final String on = "ON";
	 public static final String off = "OFF";
	 public static final String volumeRadio = "VOLRadio";
	 public static final String statusVolumeRadio = "VOLRadiostatus";
	 public static final String volumeTv = "VOLTv";
	 public static final String statusVolumeTv = "VOLTVstatus";
	 public static final String temperatura = "TEMP";
	 public static final String statusTemperatura = "TEMPstatus";
	 public static final String estacao = "ESTACAO";
	 public static final String statusEstacao = "ESTACAOstatus";
	 public static final String canal = "CANAL";
	 public static final String statusCanal = "CANALstatus";
}
