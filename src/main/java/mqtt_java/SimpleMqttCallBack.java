package mqtt_java;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class SimpleMqttCallBack implements MqttCallback {
	
	String message;

  @Override
public void connectionLost(Throwable throwable) {
    System.out.println("Connection to MQTT broker lost!");
  }

  @Override
public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
	message =   new String(mqttMessage.getPayload());
    System.out.println("Message received" + " to " + topic + " :\t"+ message );
    
  }

  @Override
public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
  }
  
  public String getMessage(){
	  
	return message;
	  
  }
}
