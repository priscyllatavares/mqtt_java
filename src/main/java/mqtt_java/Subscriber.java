package mqtt_java;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

public class Subscriber {

  public static void main(String[] args) throws MqttException {

    System.out.println("== START SUBSCRIBER ==");
    String broker = "tcp://localhost:1883"; 
    
    String lampId = MqttClient.generateClientId(); // ou lampId = "lampada" ;
    new Subscriber().receiveMessage(broker, lampId, "LAMP");
    
    String tvId = MqttClient.generateClientId(); // ou tvId = "televisao";
    new Subscriber().receiveMessage(broker, tvId, "TV");
    
  }
  
  /** Recebe mensagem de acordo com o t�pico
  *
  * @param objeto , no nosso caso o tipo de objetos: LAMP, TV, RADIO, AIRCONDITIONER, CURTAIN;
  * @param broker , o endere�o do broker
  * @param clientId , uma id do cliente aleat�ria ou fixa, mas deve ser �nica no broker, caso contr�rio d� erro.
  */
  public void receiveMessage(String broker, String clientId, String objeto){
	  	//Pode gerar uma id aleat�ria (String clientId = MqttClient.generateClientId();) ou uma id fixa, mas n�o pode ter id repetida no mesmo Broker
		try {
			MqttClient client = new MqttClient(broker, clientId);
			 client.setCallback( new SimpleMqttCallBack() );
			 client.connect();
			 client.subscribe(objeto);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//"tcp://localhost:1883"
	    System.out.println("ClientId: " + clientId);
	   
  }
  
  
  
  /** Recebe mensagem de acordo com o t�pico
  *
  * @param objeto , no nosso caso o tipo de objetos: LAMP, TV, RADIO, AIRCONDITIONER, CURTAIN;
  * @param broker , o endere�o do broker
  * @param clientId , uma id do cliente aleat�ria ou fixa, mas deve ser �nica no broker, caso contr�rio d� erro.
  */
  public void receiveMessageString(String broker, String clientId, String objeto){
	  	//Pode gerar uma id aleat�ria (String clientId = MqttClient.generateClientId();) ou uma id fixa, mas n�o pode ter id repetida no mesmo Broker
		try {
			MqttClient client = new MqttClient(broker, clientId);
			 SimpleMqttCallBack sc = new SimpleMqttCallBack();
			 client.setCallback( sc );
			 client.connect();
			 client.subscribe(objeto);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//"tcp://localhost:1883"
	    System.out.println("ClientId: " + clientId);
		
	   
  }

}
