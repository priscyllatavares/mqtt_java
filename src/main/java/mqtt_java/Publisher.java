package mqtt_java;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import javax.swing.JOptionPane;
public class Publisher {

	//MqttPublishSample
	public static void main(String[] args) {

       
        String broker       = "tcp://localhost:1883";//"tcp://test.mosquitto.org";//"tcp://iot.eclipse.org:1883";
        //Pode gerar uma id aleat�ria (String clientId = MqttClient.generateClientId();) ou uma id fixa, mas n�o pode ter id repetida no mesmo Broker
        String clientId     = "Objeto";
        
        int vezesChamaDialog = 3;
        while ( vezesChamaDialog >= 1){
        
        String message = JOptionPane.showInputDialog("Qual a mensagem? Ex.: LAMP off");
        //S� uma maneira r�pida de testar o tipo de objeto e menssagem
        //ex: LAMP off
        String []temp = message.split(" ");
        
        String messageContent = temp[1];
        String topic = temp[0];
        
        
        new Publisher().sendMessage( topic, messageContent,broker,clientId);
        vezesChamaDialog--;
        }
        
 
    }
    
    /** Manda mensagem para quem estiver ouvindo o t�pico
     *
     * @param topic , no nosso caso o tipo de objetos: LAMP, TV, RADIO, AIRCONDITIONER, CURTAIN;
     * @param content , a mensagem, no nosso caso ON, OFF, etc;
     * @param broker , o endere�o do broker
     * @param clientId , uma id do cliente aleat�ria ou fixa, mas deve ser �nica no broker, caso contr�rio d� erro.
     */
    public void sendMessage(String topic, String content,String broker, String clientId ){
        int qos = 2;
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            MqttClient objectClient = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true); //it is safe to use memory persistence as all state it cleared when a client disconnects
            System.out.println("Connecting to broker: "+ broker);
            objectClient.connect(connOpts);
            System.out.println("Connected");
            System.out.println("Publishing message: " + content);
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(qos);
            objectClient.publish(topic, message);
            System.out.println("Message published");
            objectClient.disconnect();
            System.out.println("Disconnected");
           //  System.exit(0);
            
                       
            
        } catch(MqttException me) {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            me.printStackTrace();
        }
    }
}
