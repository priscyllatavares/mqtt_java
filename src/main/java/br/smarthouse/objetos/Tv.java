package br.smarthouse.objetos;

public class Tv{

	private int canalSelecionado = 20;
	private int volumeSelecionado = 15;
	private boolean ligar = false;
	

	public boolean isLigar() {
		return ligar;
	}

	public void setLigar(boolean ligar) {
		this.ligar = ligar;
	}

	public int getCanalSelecionado() {
		return canalSelecionado;
	}

	public void setCanalSelecionado(int canalSelecionado) {
		this.canalSelecionado = canalSelecionado;
	}

	public int getVolumeSelecionado() {
		return volumeSelecionado;
	}

	public void setVolumeSelecionado(int volumeSelecionado) {
		this.volumeSelecionado = volumeSelecionado;
	}

}
