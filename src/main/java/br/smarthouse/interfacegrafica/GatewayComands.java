package br.smarthouse.interfacegrafica;

import javax.swing.JOptionPane;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import util.Constants;

public class GatewayComands implements MqttCallback {

	String message;

	// MqttPublishSample
	public static void main(String[] args) {

		String broker = "tcp://localhost:1883";// "tcp://test.mosquitto.org";//"tcp://iot.eclipse.org:1883";
		// Pode gerar uma id aleat�ria (String clientId =
		// MqttClient.generateClientId();) ou uma id fixa, mas n�o pode ter id
		// repetida no mesmo Broker
		String clientId = "InterfaceGrafica";
		MqttClient objectClient;

		// ************************ COMANDOS PARA A INTERFACE GR�FICA
		// ************************//
		// OBS:Adicionar classe Constants no projeto.
		GatewayComands gc = new GatewayComands();

		objectClient = gc.createClient(broker, clientId);
				
		//gc.sendMsgON(objectClient, Constants.lampada);
	//	gc.sendMsgOFF(objectClient, Constants.lampada);
		
		//gc.sendMsgON(objectClient, Constants.cortina);
		//gc.sendMsgOFF(objectClient, Constants.cortina);
	
	//	gc.sendMsgON(objectClient, Constants.radio);
	//	gc.sendMsgOFF(objectClient, Constants.radio);
	//	gc.sendMsgNumero(objectClient, Constants.volumeRadio, "78");
	//	gc.sendMsgNumero(objectClient, Constants.estacao, "55.9");
		
		gc.sendMsgON(objectClient, Constants.arcondicionado);
//		gc.sendMsgOFF(objectClient, Constants.arcondicionado);
		gc.sendMsgNumero(objectClient, Constants.temperatura, "21");
		
//		gc.sendMsgON(objectClient, Constants.tv);
//		gc.sendMsgOFF(objectClient, Constants.tv);
//		gc.sendMsgNumero(objectClient, Constants.volumeTv, "33");
//		gc.sendMsgNumero(objectClient, Constants.canal, "10");
		
		
		gc.receiveMessage(objectClient, Constants.statusLampada, Constants.statusCortina, Constants.statusTv, Constants.statusArcondicionado,
				Constants.statusRadio, Constants.statusVolumeRadio, Constants.statusEstacao, Constants.statusVolumeTv,Constants.statusCanal, Constants.statusTemperatura);

		//String message = JOptionPane.showInputDialog("Mardar ligar lampada.");
		//gc.sendMsgON(objectClient, Constants.lampada);
		//gc.sendMsgLampadaON(objectClient);

		// ************************ COMANDOS PARA A INTERFACE GR�FICA
		// ************************//

		// String message = JOptionPane.showInputDialog("Mardar ligar
		// lampada.");
		// new GatewayComands().sendMsgLampadaON( broker, clientId );
		// new GatewayComands().receiveMessage(broker, clientId,
		// Constants.lampada, Constants.cortina, Constants.tv,
		// Constants.arcondicionado, Constants.radio);

		/*
		 * int vezesChamaDialog = 3; while ( vezesChamaDialog >= 1){
		 * 
		 * String message =
		 * JOptionPane.showInputDialog("Qual a mensagem? Ex.: LAMP off"); //S�
		 * uma maneira r�pida de testar o tipo de objeto e menssagem //ex: LAMP
		 * off String []temp = message.split(" ");
		 * 
		 * String messageContent = temp[1]; String topic = temp[0];
		 * 
		 * new PublishComands().sendMessage( topic,
		 * messageContent,broker,clientId); vezesChamaDialog--; }
		 */

	}

	public MqttClient createClient(String broker, String clientId) {

		MemoryPersistence persistence = new MemoryPersistence();
		MqttClient objectClient = null;
		try {
			objectClient = new MqttClient(broker, clientId, persistence);

			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true); // it is safe to use memory
											// persistence as all state it
											// cleared when a client disconnects
			System.out.println("Connecting to broker: " + broker);
			objectClient.connect(connOpts);
			objectClient.setCallback(this);
			System.out.println("Connected");
			System.out.println("ClientId: " + clientId);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return objectClient;

	}

	public void sendMsgON(MqttClient objectClient, String topic) {
		int qos = 2;
		String content = Constants.on;

		try {

			System.out.println("Publishing message: " + content);
			MqttMessage message = new MqttMessage(content.getBytes());
			message.setQos(qos);
			objectClient.publish(topic, message);
			System.out.println("Message published");
			System.out.println();
			// objectClient.disconnect();
			// System.out.println("Disconnected");
			// System.exit(0);

		} catch (MqttException me) {
			System.out.println("reason " + me.getReasonCode());
			System.out.println("msg " + me.getMessage());
			System.out.println("loc " + me.getLocalizedMessage());
			System.out.println("cause " + me.getCause());
			System.out.println("excep " + me);
			me.printStackTrace();
		}
	}

	public void sendMsgOFF(MqttClient objectClient, String topic) {
		int qos = 2;
		String content = Constants.off;

		try {

			System.out.println("Publishing message: " + content);
			MqttMessage message = new MqttMessage(content.getBytes());
			message.setQos(qos);
			objectClient.publish(topic, message);
			System.out.println("Message published");
			System.out.println();
			// objectClient.disconnect();
			// System.out.println("Disconnected");

		} catch (MqttException me) {
			System.out.println("reason " + me.getReasonCode());
			System.out.println("msg " + me.getMessage());
			System.out.println("loc " + me.getLocalizedMessage());
			System.out.println("cause " + me.getCause());
			System.out.println("excep " + me);
			me.printStackTrace();
		}

	}

	
	public void sendMsgNumero(MqttClient objectClient, String topic, String numero) {
		int qos = 2;
		
		try {

			System.out.println("Publishing message: " + numero);
			MqttMessage message = new MqttMessage(numero.getBytes());
			message.setQos(qos);
			objectClient.publish(topic, message);
			System.out.println("Message published");
			System.out.println();
			// objectClient.disconnect();
			// System.out.println("Disconnected");
			// System.exit(0);

		} catch (MqttException me) {
			System.out.println("reason " + me.getReasonCode());
			System.out.println("msg " + me.getMessage());
			System.out.println("loc " + me.getLocalizedMessage());
			System.out.println("cause " + me.getCause());
			System.out.println("excep " + me);
			me.printStackTrace();
		}
	}
	

	// ***************FUN��ES NECESS�RIAS PARA RECEBER************//

	@Override
	public void connectionLost(Throwable throwable) {
		System.out.println("Connection to MQTT broker lost!");
	}

	@Override
	public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
		message = new String(mqttMessage.getPayload());
		System.out.println("Message received" + " to " + topic + " :\t" + message);
		System.out.println();

		//Tem que fazer de acordo com a Interface;
		atualizaObjeto(message, topic);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

	}

	public void receiveMessage(MqttClient client, String objeto1, String objeto2, String objeto3, String objeto4,
			String objeto5, String objeto6,String objeto7,String objeto8,String objeto9,String objeto10 ) {
		// Pode gerar uma id aleat�ria (String clientId =
		// MqttClient.generateClientId();) ou uma id fixa, mas n�o pode ter id
		// repetida no mesmo Broker
		try {

			client.subscribe(objeto1);
			client.subscribe(objeto2);
			client.subscribe(objeto3);
			client.subscribe(objeto4);
			client.subscribe(objeto5);
			client.subscribe(objeto6);
			client.subscribe(objeto7);
			client.subscribe(objeto8);
			client.subscribe(objeto9);
			client.subscribe(objeto10);

		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // "tcp://localhost:1883"

	}
	
	private void atualizaObjeto(String message, String topic) {


	}

	public void terminarConexao(MqttClient client) {
		try {
			client.disconnect();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Disconnected");
	}

	// *******************************************************************//
}
