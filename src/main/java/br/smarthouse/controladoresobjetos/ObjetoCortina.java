package br.smarthouse.controladoresobjetos;

import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import br.smarthouse.objetos.Cortina;
import br.smarthouse.objetos.Lampada;
import mqtt_java.Subscriber;
import util.Constants;

public class ObjetoCortina implements MqttCallback {

	String message;
	Cortina cortina;
	MqttClient objectClient;
	int delay = 5000; // delay de 5 seg.
	int interval = 10000; // intervalo de 1 seg.
	
	public ObjetoCortina() {

		System.out.println("== START SUBSCRIBER CORTINA ==");
		final String broker = "tcp://localhost:1883";
		final String clientId = "cortina";

		cortina = new Cortina();
		objectClient = createClient(broker, clientId);
		receiveMessage(objectClient, Constants.cortina);

		
		Timer timer = new Timer();

		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				if (cortina.isLevantar()) {

					sendMessage(objectClient, Constants.on, broker, clientId);
				} else {
					sendMessage(objectClient, Constants.off, broker, clientId);
				}

			}
		}, delay, interval);

	}

	public MqttClient createClient(String broker, String clientId) {

		MemoryPersistence persistence = new MemoryPersistence();
		MqttClient objectClient = null;
		try {
			objectClient = new MqttClient(broker, clientId, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true); // it is safe to use memory
											// persistence as all state it
											// cleared when a client disconnects
			System.out.println("Connecting to broker: " + broker);
			objectClient.connect(connOpts);
			objectClient.setCallback(this);
			System.out.println("Connected");
			System.out.println("ClientId: " + clientId);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return objectClient;

	}

	public void receiveMessage(MqttClient client, String objeto) {
		// Pode gerar uma id aleat�ria (String clientId =
		// MqttClient.generateClientId();) ou uma id fixa, mas n�o pode ter id
		// repetida no mesmo Broker
		try {
			client.subscribe(objeto);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // "tcp://localhost:1883"

	}

	public void sendMessage(MqttClient client, String content, String broker, String clientId) {
		int qos = 2;

		try {
			System.out.println("Publishing message from Cortina: " + content);
			MqttMessage message = new MqttMessage(content.getBytes());
			message.setQos(qos);
			client.publish(Constants.statusCortina, message);
			System.out.println("Message published");
			/*
			 * objectClient.disconnect(); System.out.println("Disconnected");
			 */
			System.out.println();
			// System.exit(0);

		} catch (MqttException me) {
			System.out.println("reason " + me.getReasonCode());
			System.out.println("msg " + me.getMessage());
			System.out.println("loc " + me.getLocalizedMessage());
			System.out.println("cause " + me.getCause());
			System.out.println("excep " + me);
			me.printStackTrace();
		}
	}

	@Override
	public void connectionLost(Throwable throwable) {
		System.out.println("Connection to MQTT broker lost!");
	}

	@Override
	public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
		message = new String(mqttMessage.getPayload());
		System.out.println("Message received" + " to " + topic + " Cortina" + " :\t" + message);

		atualizaObjeto(message);

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

	}

	private void atualizaObjeto(String message) {

		switch (message) {
		case Constants.on:
			cortina.setLevantar(true);
			System.out.println("Levanta cortina");
			break;
		case Constants.off:
			cortina.setLevantar(false);
			System.out.println("Baixa cortina");
			break;
		default:
			break;
		}

	}

}
