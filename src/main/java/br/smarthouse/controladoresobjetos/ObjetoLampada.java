package br.smarthouse.controladoresobjetos;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import br.smarthouse.objetos.Lampada;
import mqtt_java.Subscriber;
import util.Constants;

public class ObjetoLampada implements MqttCallback {

	String message;
	Lampada lampada;
	MqttClient objectClient;
	int delay = 5000; // delay de 5 seg.
	int interval = 10000; // intervalo de 1 seg.

	public ObjetoLampada() {

		System.out.println("== START SUBSCRIBER LAMPADA ==");
		final String broker = "tcp://localhost:1883";
		final String lampId = "lampada"; // ou lampId = "lampada" ou
											// MqttClient.generateClientId()

		lampada = new Lampada();
		objectClient = createClient(broker, lampId);

		receiveMessage(objectClient, Constants.lampada);

		Timer timer = new Timer();

		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				if (lampada.isLigar()) {

					sendMessage(objectClient, Constants.on, broker, lampId);
				} else {
					sendMessage(objectClient, Constants.off, broker, lampId);
				}

			}
		}, delay, interval);

	}

	public MqttClient createClient(String broker, String clientId) {

		MemoryPersistence persistence = new MemoryPersistence();
		MqttClient objectClient = null;
		try {
			objectClient = new MqttClient(broker, clientId, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true); // it is safe to use memory
											// persistence as all state it
											// cleared when a client disconnects
			System.out.println("Connecting to broker: " + broker);
			objectClient.connect(connOpts);
			objectClient.setCallback(this);
			System.out.println("Connected");
			System.out.println("ClientId: " + clientId);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return objectClient;

	}

	@Override
	public void connectionLost(Throwable throwable) {
		System.out.println("Connection to MQTT broker lost!");
	}

	@Override
	public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
		message = new String(mqttMessage.getPayload());
		System.out.println("Message received" + " to " + topic + " Lampada" + " :\t" + message);
		atualizaObjeto(message);

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

	}

	private void atualizaObjeto(String message) {

		switch (message) {
		case Constants.on:
			lampada.setLigar(true);
			System.out.println("Acende Luz");
			System.out.println();
			break;
		case Constants.off:
			lampada.setLigar(false);
			System.out.println("Apaga Luz");
			System.out.println();
		default:
			break;
		}

	}

	/**
	 * Recebe mensagem de acordo com o t�pico
	 *
	 * @param objeto
	 *            , no nosso caso o tipo de objetos: LAMP, TV, RADIO,
	 *            AIRCONDITIONER, CURTAIN;
	 * @param broker
	 *            , o endere�o do broker
	 * @param clientId
	 *            , uma id do cliente aleat�ria ou fixa, mas deve ser �nica no
	 *            broker, caso contr�rio d� erro.
	 */
	public void receiveMessage(MqttClient client, String objeto) {
		// Pode gerar uma id aleat�ria (String clientId =
		// MqttClient.generateClientId();) ou uma id fixa, mas n�o pode ter id
		// repetida no mesmo Broker
		try {
			client.subscribe(objeto);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // "tcp://localhost:1883"

	}

	public void sendMessage(MqttClient client, String content, String broker, String clientId) {
		int qos = 2;

		try {
			System.out.println("Publishing message from Lampada: " + content);
			MqttMessage message = new MqttMessage(content.getBytes());
			message.setQos(qos);
			client.publish(Constants.statusLampada, message);
			System.out.println("Message published");
			/*
			 * objectClient.disconnect(); System.out.println("Disconnected");
			 */
			System.out.println();
			// System.exit(0);

		} catch (MqttException me) {
			System.out.println("reason " + me.getReasonCode());
			System.out.println("msg " + me.getMessage());
			System.out.println("loc " + me.getLocalizedMessage());
			System.out.println("cause " + me.getCause());
			System.out.println("excep " + me);
			me.printStackTrace();
		}
	}
}
