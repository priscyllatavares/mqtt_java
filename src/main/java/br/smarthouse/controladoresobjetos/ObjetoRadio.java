package br.smarthouse.controladoresobjetos;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import br.smarthouse.objetos.Radio;
import util.Constants;

public class ObjetoRadio implements MqttCallback {

	String message;
	Radio radio;
	MqttClient objectClient;
	int delay = 5000; // delay de 5 seg.
	int interval = 10000; // intervalo de 1 seg.

	public ObjetoRadio() {

		System.out.println("== START SUBSCRIBER RADIO ==");
		final String broker = "tcp://localhost:1883";
		final String radioId = "radio"; 

		radio = new Radio();
		objectClient = createClient(broker, radioId);

		receiveMessage(objectClient, Constants.radio, Constants.estacao, Constants.volumeRadio);

		Timer timer = new Timer();

		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				if (radio.isLigar()) {

					sendMessage(objectClient, Constants.on, broker, radioId, Constants.statusRadio);
				} else {
					sendMessage(objectClient, Constants.off, broker, radioId, Constants.statusRadio);
				}
				
				sendMessage(objectClient, radio.getEstacaoSelecionada() +"", broker, radioId, Constants.statusEstacao);
				sendMessage(objectClient, radio.getVolumeSelecionado() +"", broker, radioId, Constants.statusVolumeRadio);
			}
		}, delay, interval);

	}

	public MqttClient createClient(String broker, String clientId) {

		MemoryPersistence persistence = new MemoryPersistence();
		MqttClient objectClient = null;
		try {
			objectClient = new MqttClient(broker, clientId, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true); // it is safe to use memory
											// persistence as all state it
											// cleared when a client disconnects
			System.out.println("Connecting to broker: " + broker);
			objectClient.connect(connOpts);
			objectClient.setCallback(this);
			System.out.println("Connected");
			System.out.println("ClientId: " + clientId);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return objectClient;

	}

	@Override
	public void connectionLost(Throwable throwable) {
		System.out.println("Connection to MQTT broker lost!");
	}

	@Override
	public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
		message = new String(mqttMessage.getPayload());
		System.out.println("Message received" + " to " + topic + " Radio" + ":\t" + message);
		atualizaObjeto(message, topic);

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

	}

	private void atualizaObjeto(String message, String topic) {

		switch (message) {
		case Constants.on:
			radio.setLigar(true);
			System.out.println("Liga Radio");
			System.out.println();
			break;
		case Constants.off:
			radio.setLigar(false);
			System.out.println("Desliga Radio");
			System.out.println();
			break;
			
		default:
			break;
		}
		
		switch (topic) {
		case Constants.estacao:
			float estacao = Float.parseFloat(message);
			radio.setEstacaoSelecionada(estacao);
			System.out.println("Estacao Radio: " + estacao);
			System.out.println();
			break;
		case Constants.volumeRadio:
			int vol = Integer.valueOf(message);
			radio.setVolumeSelecionado(vol);
			System.out.println("Volume Radio " + vol);
			System.out.println();
			break;
		default:
			break;
		}

	}

	
	public void receiveMessage(MqttClient client, String topico1, String topico2, String topico3) {
		// Pode gerar uma id aleat�ria (String clientId =
		// MqttClient.generateClientId();) ou uma id fixa, mas n�o pode ter id
		// repetida no mesmo Broker
		try {
			client.subscribe(topico1);
			client.subscribe(topico2);
			client.subscribe(topico3);
			
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // "tcp://localhost:1883"

	}
	
	

	public void sendMessage(MqttClient client, String content, String broker, String clientId, String topic) {
		int qos = 2;

		try {
			System.out.println("Publishing message from Radio: " + content);
			MqttMessage message = new MqttMessage(content.getBytes());
			message.setQos(qos);
			client.publish(topic, message);
			System.out.println("Message published");
			/*
			 * objectClient.disconnect(); System.out.println("Disconnected");
			 */
			System.out.println();
			// System.exit(0);

		} catch (MqttException me) {
			System.out.println("reason " + me.getReasonCode());
			System.out.println("msg " + me.getMessage());
			System.out.println("loc " + me.getLocalizedMessage());
			System.out.println("cause " + me.getCause());
			System.out.println("excep " + me);
			me.printStackTrace();
		}
	}
}
