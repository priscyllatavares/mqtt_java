package br.smarthouse.controladoresobjetos;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import br.smarthouse.objetos.Arcondicionado;
import util.Constants;

public class ObjetoArcondicionado implements MqttCallback {

	String message;
	Arcondicionado arcondicionado;
	MqttClient objectClient;
	int delay = 5000; // delay de 5 seg.
	int interval = 10000; // intervalo de 1 seg.

	public ObjetoArcondicionado() {

		System.out.println("== START SUBSCRIBER ARCONDICIONADO ==");
		final String broker = "tcp://localhost:1883";
		final String arCondId = "arCond"; 

		arcondicionado = new Arcondicionado();
		objectClient = createClient(broker, arCondId);

		receiveMessage(objectClient, Constants.arcondicionado, Constants.temperatura);

		Timer timer = new Timer();

		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				if (arcondicionado.isLigar()) {

					sendMessage(objectClient, Constants.on, broker, arCondId, Constants.statusArcondicionado);
				} else {
					sendMessage(objectClient, Constants.off, broker, arCondId, Constants.statusArcondicionado);
				}
				
				sendMessage(objectClient, arcondicionado.getTemperaturaProgramada()+"", broker, arCondId, Constants.statusTemperatura);

			}
		}, delay, interval);

	}

	public MqttClient createClient(String broker, String clientId) {

		MemoryPersistence persistence = new MemoryPersistence();
		MqttClient objectClient = null;
		try {
			objectClient = new MqttClient(broker, clientId, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true); // it is safe to use memory
											// persistence as all state it
											// cleared when a client disconnects
			System.out.println("Connecting to broker: " + broker);
			objectClient.connect(connOpts);
			objectClient.setCallback(this);
			System.out.println("Connected");
			System.out.println("ClientId: " + clientId);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return objectClient;

	}

	@Override
	public void connectionLost(Throwable throwable) {
		System.out.println("Connection to MQTT broker lost!");
	}

	@Override
	public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
		message = new String(mqttMessage.getPayload());
		System.out.println("Message received" + " to " + topic + " Ar Condicionado" + " :\t" + message);
		atualizaObjeto(message, topic);

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

	}

	private void atualizaObjeto(String message, String topic) {

		
			switch (message) {
			case Constants.on:
				arcondicionado.setLigar(true);
				System.out.println("Liga Ar");
				System.out.println();
				break;
			case Constants.off:
				arcondicionado.setLigar(false);
				System.out.println("Desliga ar");
				System.out.println();
				break;
			default:
				break;
			}
			
			if(topic.equals(Constants.temperatura)){
				int temp = Integer.valueOf(message);
				arcondicionado.setTemperaturaProgramada(temp);
				System.out.println("Temperatura do ar " + temp + "�C");
				System.out.println();
			}
				

	}

	
	public void receiveMessage(MqttClient client, String objeto1, String objeto2) {
		// Pode gerar uma id aleat�ria (String clientId =
		// MqttClient.generateClientId();) ou uma id fixa, mas n�o pode ter id
		// repetida no mesmo Broker
		try {
			client.subscribe(objeto1);
			client.subscribe(objeto2);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // "tcp://localhost:1883"

	}

	public void sendMessage(MqttClient client, String content, String broker, String clientId, String topic) {
		int qos = 2;

		try {
			System.out.println("Publishing message from Ar Condicionado: " + content);
			MqttMessage message = new MqttMessage(content.getBytes());
			message.setQos(qos);
			client.publish(topic, message);
			System.out.println("Message published");
			/*
			 * objectClient.disconnect(); System.out.println("Disconnected");
			 */
			System.out.println();
			// System.exit(0);

		} catch (MqttException me) {
			System.out.println("reason " + me.getReasonCode());
			System.out.println("msg " + me.getMessage());
			System.out.println("loc " + me.getLocalizedMessage());
			System.out.println("cause " + me.getCause());
			System.out.println("excep " + me);
			me.printStackTrace();
		}
	}
}
